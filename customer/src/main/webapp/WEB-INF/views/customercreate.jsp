<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Power Capital Systems - Customer Management</title>
<link href="resources/fonts.css" rel="stylesheet" type="text/css" />
<link href="resources/style.css" rel="stylesheet" type="text/css" media="screen" />

</head>
<body>
	<form>
	<div class="container">
	   <table>
	   <tr><td>Dealer Name</td></tr>
	   <tr><td><input type="text"/></td></tr>
	   <tr><td><font color="red">Customer Name*</font></td></tr>
	   <tr><td><input type="text"/></td></tr>
	   <tr><td>Address</td></tr>
	   <tr><td><textarea></textarea></td></tr>
	   <tr><td>Email Id <input type="text"/></td></tr>
	   <tr><td>Date Of Order</td><td>Date Of Supply</td><td>Date Of Installation</td></tr>
	   <tr><td><input type="text"/></td><td><input type="text"/></td><td><input type="text"/></td></tr>
	   <tr><td>Bill no.</td><td>Order Form no.</td><td>Contact no.</td></tr>
	   <tr><td><input type="text"/></td><td><input type="text"/></td><td><input type="text"/></td></tr>
	   <tr><td></td></tr>
	   <tr><td></td></tr>
	   
	   <tr><td><strong>**********************</strong><td><strong>Price details</strong></td><td><strong>**********************</strong></td></tr>
	   <tr><td>Unit Rate</td><td>Quantity</td><td>VAT%</td><td>Discount</td><td>Total</td><td>Balance</td></tr>
	   <tr><td><input type="text"/></td><td><input type="text"/></td><td><input type="text"/></td><td><input type="text"/></td><td><input type="text"/></td><td><input type="text"/></td></tr>
	   <tr><td></td></tr>
	   <tr><td></td></tr>
	   <tr><td><strong>---------------------</strong></td><td><strong>Products</strong></td><td><strong>---------------------</strong></td></tr>
	   <tr></tr>
	   <tr><td>Solar Capacity</td><td>Model</td><td>O Cladding</td><td>Warranty</td><td>Free Service</td></tr>
	   <tr>
		   <td><select>
		   		<option>100</option>
		   		<option>150</option>
		   		<option>200</option>
		   		<option>250</option>
		   		<option>300</option>
		   		<option>500</option>
		   		<option>1000</option>
		   	   </select>
		   </td>
		   <td><select>
		   		<option>ETC</option>
		   		<option>FPC</option>
		   		
		   	   </select>
		   </td>
		   <td><select>
		   		<option>SS</option>
		   		<option>PC</option>
		   		
		   	   </select>
		   </td>
		   <td><select>
		   		<option>1</option>
		   		<option>2</option>
		   		<option>3</option>
		   		<option>4</option>
		   		<option>5</option>
		   		<option>6</option>
		   		<option>7</option>
		   		<option>8</option>
		   		<option>9</option>
		   		<option>10</option>
		   	   </select>
		   </td>
		   <td><select>
		   		<option>1</option>
		   		<option>2</option>
		   		<option>3</option>
		   		<option>4</option>
		   		<option>5</option>
		   		<option>6</option>
		   		<option>7</option>
		   		<option>8</option>
		   		<option>9</option>
		   		<option>10</option>
		   	   </select>
		   </td>
		   
	   </tr>
	   <tr><td>Capital water purifiers</td><td>Warranty</td><td>Free Service</td></tr>
	   <tr>
		   <td><select>
		   		<option>Ultra Pure</option>
		   		<option>Compaq UV</option>
		   		<option>UV 100LPH</option>
		   		<option>UV 100LPH with</option>
		   		<option>50ltrs SS Tank Storage</option>
		   		<option>RO Compaq</option>
		   		<option>RO Pure</option>
		   		<option>RO 30 LPH</option>
		   		<option>RO 50 LPH</option>
		   		<option>RO 50 LPH with</option>
		   		<option>SS Tank Storage</option>
		   		<option>RO 100 LPH</option>
		   	   </select>
		   </td>
		    <td><select>
		   		<option>1</option>
		   		<option>2</option>
		   		<option>3</option>
		   		<option>4</option>
		   		<option>5</option>
		   		<option>6</option>
		   		<option>7</option>
		   		<option>8</option>
		   		<option>9</option>
		   		<option>10</option>
		   	   </select>
		   </td>
		   <td><select>
		   		<option>1</option>
		   		<option>2</option>
		   		<option>3</option>
		   		<option>4</option>
		   		<option>5</option>
		   		<option>6</option>
		   		<option>7</option>
		   		<option>8</option>
		   		<option>9</option>
		   		<option>10</option>
		   	   </select>
		   </td>
		   </tr>
		   
	   <tr><td>Capital LED Light</td><td>Model</td><td>Warranty</td><td>Free Service</td></tr>
	   <tr>
		   <td><select>
		   		<option>3W</option>
		   		<option>6W</option>
		   		<option>9W</option>
		   		<option>12W</option>
		   		<option>15W</option>
		   		<option>18W</option>
		   		<option>21W</option>
		   		<option>24W</option>
		   		<option>27W</option>
		   		<option>30W</option>
		   		<option>36W</option>
		   		<option>45W</option>
		   	   </select>
		   </td>
		   <td><select>
		   		<option>Indoor</option>
		   		<option>Outdoor</option>
		   		
		   	   </select>
		   </td>
			<td><select>
		   		<option>1</option>
		   		<option>2</option>
		   		<option>3</option>
		   		<option>4</option>
		   		<option>5</option>
		   		<option>6</option>
		   		<option>7</option>
		   		<option>8</option>
		   		<option>9</option>
		   		<option>10</option>
		   	   </select>
		   </td>
		   <td><select>
		   		<option>1</option>
		   		<option>2</option>
		   		<option>3</option>
		   		<option>4</option>
		   		<option>5</option>
		   		<option>6</option>
		   		<option>7</option>
		   		<option>8</option>
		   		<option>9</option>
		   		<option>10</option>
		   	   </select>
		   </td>
		   </tr>
	   <tr><td>UPS</td><td>Battery</td><td>Model</td><td>Warranty</td><td>Free Service</td></tr>
	   <tr>
		   <td><select>
		   		<option>600 VA</option>
		   		<option>850 VA</option>
		   		<option>1 KVA</option>
		   		<option>1.5 KVA</option>
		   		<option>2 KVA</option>
		   		<option>2.5 KVA</option>
		   		<option>3 KVA</option>
		   		<option>3.5 KVA</option>
		   		<option>5 KVA</option>
		   		<option>7 KVA</option>
		   		<option>10 KVA</option>
		   	   </select>
		   </td>
		   <td><select>
		   		<option>7.2 AH</option>
		   		<option>17 AH</option>
		   		<option>20 AH</option>
		   		<option>26 AH</option>
		   		<option>42 AH</option>
		   		<option>65 AH</option>
		   		<option>82 AH</option>
		   		<option>100 AH</option>
		   		<option>120 AH</option>
		   		<option>150 AH</option>
		   		<option>180 AH</option>
		   		<option>200 AH</option>
		   		<option>220 AH</option>
		   	   </select>
		   </td>
		   <td><select>
		   		<option>SMF</option>
		   		<option>Automotive</option>
		   		<option>Flat tubular</option>
		   		<option>Hard tubular</option>
		   		<option>Tower tubular</option>
		   		
		   	   </select>
		   </td>
			<td><select>
		   		<option>1</option>
		   		<option>2</option>
		   		<option>3</option>
		   		<option>4</option>
		   		<option>5</option>
		   		<option>6</option>
		   		<option>7</option>
		   		<option>8</option>
		   		<option>9</option>
		   		<option>10</option>
		   	   </select>
		   </td>
		   <td><select>
		   		<option>1</option>
		   		<option>2</option>
		   		<option>3</option>
		   		<option>4</option>
		   		<option>5</option>
		   		<option>6</option>
		   		<option>7</option>
		   		<option>8</option>
		   		<option>9</option>
		   		<option>10</option>
		   	   </select>
		   </td>
		   </tr>
	   <tr><td>Gas Geyser Model</td><td>Warranty</td><td>Free Service</td></tr>
	   <tr>
	   	<td><select>
		   		<option>DELUXE</option>
		   		<option>Gold</option>
		   		<option>Others</option>
		   	   </select>
		   </td>
		   <td><select>
		   		<option>1</option>
		   		<option>2</option>
		   		<option>3</option>
		   		<option>4</option>
		   		<option>5</option>
		   		<option>6</option>
		   		<option>7</option>
		   		<option>8</option>
		   		<option>9</option>
		   		<option>10</option>
		   	   </select>
		   </td>
		   <td><select>
		   		<option>1</option>
		   		<option>2</option>
		   		<option>3</option>
		   		<option>4</option>
		   		<option>5</option>
		   		<option>6</option>
		   		<option>7</option>
		   		<option>8</option>
		   		<option>9</option>
		   		<option>10</option>
		   	   </select>
		   </td>
	   </tr>
	   <tr><td>Solar Light</td><td>Battery</td><td>Battery Model</td><td>Quantity</td><td>LED lights</td><td>Quantity</td><td>Backup</td><td>Warranty</td><td>Free Service</td></tr>
	   <tr>
		   <td><select>
		   		<option>3 Light Small</option>
		   		<option>2 Light Kit</option>
		   		<option>5 Light Kit</option>
		   		<option>7 Light Kit</option>
		   		<option>10 Light Kit</option>
		   		<option>12 Light Kit</option>
		   		<option>15 Light Kit</option>
		   		<option>Street Light</option>
		   		<option>AC Light</option>
		   		<option>200W Power Pack</option>
		   		<option>500W Power Pack</option>
		   		<option>800W Power Pack</option>
		   		<option>1000W Power Pack</option>
		   		<option>1500W Power Pack</option>
		   		<option>2000W Power Pack</option>
		   	   </select>
		   </td>
		   <td><select>
		   		<option>7.2 AH</option>
		   		<option>17 AH</option>
		   		<option>20 AH</option>
		   		<option>26 AH</option>
		   		<option>42 AH</option>
		   		<option>65 AH</option>
		   		<option>82 AH</option>
		   		<option>100 AH</option>
		   		<option>120 AH</option>
		   		<option>150 AH</option>
		   		<option>180 AH</option>
		   		<option>200 AH</option>
		   		<option>220 AH</option>
		   	   </select>
		   </td>
		   <td><select>
		   		<option>SMF</option>
		   		<option>Automotive</option>
		   		<option>Flat tubular</option>
		   		<option>Hard tubular</option>
		   		<option>Tower tubular</option>
		   		
		   	   </select>
		   </td>
		   <td>
		   	<select>
		   		<option>1</option>
		   		<option>2</option>
		   		<option>3</option>
		   		<option>4</option>
		   		<option>5</option>
		   		<option>6</option>
		   		<option>7</option>
		   		<option>8</option>
		   		<option>9</option>
		   		<option>10</option>
		   		<option>11</option>
		   		<option>12</option>
		   		<option>13</option>
		   		<option>14</option>
		   		<option>15</option>
		   		<option>16</option>
		   		<option>17</option>
		   		<option>18</option>
		   		<option>19</option>
		   		<option>20</option>
		   		<option>21</option>
		   		<option>22</option>
		   		<option>23</option>
		   		<option>24</option>
		   	   </select>
		   </td>
		    <td><select>
		   		<option>3W</option>
		   		<option>6W</option>
		   		<option>9W</option>
		   		<option>12W</option>
		   		<option>15W</option>
		   		<option>18W</option>
		   		<option>21W</option>
		   		<option>24W</option>
		   		<option>27W</option>
		   		<option>30W</option>
		   		<option>36W</option>
		   		<option>45W</option>
		   	   </select>
		   </td>
		   <td><input type="text" width="10px" maxlength="4"/></td>
		     <td>
		   	<select>
		   		<option>1</option>
		   		<option>2</option>
		   		<option>3</option>
		   		<option>4</option>
		   		<option>5</option>
		   		<option>6</option>
		   		<option>7</option>
		   		<option>8</option>
		   		<option>9</option>
		   		<option>10</option>
		   		<option>11</option>
		   		<option>12</option>
		   		<option>13</option>
		   		<option>14</option>
		   		<option>15</option>
		   		<option>16</option>
		   		<option>17</option>
		   		<option>18</option>
		   		<option>19</option>
		   		<option>20</option>
		   		<option>21</option>
		   		<option>22</option>
		   		<option>23</option>
		   		<option>24</option>
		   	   </select>
		   </td>
			<td><select>
		   		<option>1</option>
		   		<option>2</option>
		   		<option>3</option>
		   		<option>4</option>
		   		<option>5</option>
		   		<option>6</option>
		   		<option>7</option>
		   		<option>8</option>
		   		<option>9</option>
		   		<option>10</option>
		   	   </select>
		   </td>
		   <td><select>
		   		<option>1</option>
		   		<option>2</option>
		   		<option>3</option>
		   		<option>4</option>
		   		<option>5</option>
		   		<option>6</option>
		   		<option>7</option>
		   		<option>8</option>
		   		<option>9</option>
		   		<option>10</option>
		   	   </select>
		   </td>
		   </tr>
	   </table>
	   <br><br>
	   <tr><td><button type="submit">Submit</button></td><td><button type="reset">Reset</button></td><button type="button">Print</button></tr>
	</div>
	
	</form>
</body>
</html>